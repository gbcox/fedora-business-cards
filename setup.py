from setuptools import setup, find_packages

setup(
    name = 'fedora-business-cards',
    version = '2.2',
    packages = find_packages(),

    author = 'Brian Exelbierd',
    author_email = 'bexelbie@redhat.com',
    url = 'https://fedoraproject.org/wiki/Business_cards',

    include_package_data=True,

    entry_points = {
        'console_scripts': [
            ('fedora-business-cards = '
             'fedora_business_cards.frontend.cmdline:main'),
        ],
    },
)
