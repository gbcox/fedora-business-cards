###
# fedora-business-cards - for rendering Fedora contributor business cards
# Copyright (C) 2012  Red Hat, Inc. and others.
# Primary maintainer: Ian Weller <iweller@redhat.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
###

"""
Command-line interface to business card generator.
"""

from copy import copy
import decimal
import argparse
import sys

from fedora_business_cards import common
from fedora_business_cards import export  # hah
from fedora_business_cards import generators


def main():
    """
    Call this to make things happen.
    """
    # Setup option parser
    parser = argparse.ArgumentParser()
    # General options
    parser.add_argument("--list-generators", dest="showgen", default=False,
                        action="store_true", help="display list of generators")
    # Size options
    size_group = parser.add_argument_group("Size options")
    size_group.add_argument("--height", dest="height",
                            default=decimal.Decimal("2"), type=decimal.Decimal,
                            help="business card height (default: 2)")
    size_group.add_argument("--width", dest="width",
                            default=decimal.Decimal("3.5"), type=decimal.Decimal,
                            help="business card width (default: 3.5)")
    size_group.add_argument("--bleed", dest="bleed", type=decimal.Decimal,
                            default=decimal.Decimal("0"), help="extra space "
                            "around card, often requested by printers "
                            "(default: 0)")
    size_group.add_argument("--inch", dest="unit", default="in", const="in",
                            action="store_const",
                            help="units are specified in inches (default)")
    size_group.add_argument("--mm", dest="unit", default="in", const="mm",
                            action="store_const",
                            help="units are specified in millimeters")
    # Output options
    out_group = parser.add_argument_group("Output options")
    out_group.add_argument("-d", "--dpi", dest="dpi", default=300, type=int,
                           help="DPI of exported file")
    out_group.add_argument("--pdf", dest="output", default="png", const="pdf",
                           action="store_const", help="Export as PDF")
    out_group.add_argument("--png", dest="output", default="png", const="png",
                           action="store_const", help="Export as PNG (default)")
    out_group.add_argument("--svg", dest="output", default="png", const="svg",
                           action="store_const", help="Export as SVG")
    out_group.add_argument("--eps", dest="output", default="png", const="eps",
                           action="store_const", help="Export as EPS")
    out_group.add_argument("--cmyk-pdf", dest="output", default="png",
                           const="cmyk_pdf", action="store_const",
                           help="Export as PDF with CMYK color (if the generator"
                           " supports it)")

    # Check for generator-specific option groups
    subparsers = parser.add_subparsers(metavar='GENERATOR',dest='generator', required='--list-generators' not in sys.argv)
    for module_name in generators.__all__:
        try:
            module = common.recursive_import(
                'fedora_business_cards.generators.%s' % module_name)
            generator = module.generator
            generator.extra_options(subparsers)
        except ImportError:
            pass

    # Parse arguments
    options = parser.parse_args()

    if options.showgen:
        print("Generators: %s" % ', '.join(generators.__all__))
        sys.exit()

    # Import the generator we care abuot
    try:
        module = common.recursive_import('fedora_business_cards.generators.%s'
                                         % options.generator)
    except ImportError:
        parser.error("Generator '%s' does not exist or is broken" % options.generator)
    gen = module.generator(options)

    # collect information from user if necessary
    gen.collect_information()

    # generate front of business card
    print("Generating front...")
    xml = gen.generate_front()
    if options.output == "svg":
        export.svg_to_file(xml, 'front.' + options.output)
    elif options.output == "cmyk_pdf":
        export.svg_to_cmyk_pdf(xml, 'front.pdf', options.height, options.width,
                               options.bleed, options.unit, gen.rgb_to_cmyk)
    else:
        export.svg_to_pdf_png(xml, 'front.' + options.output, options.output,
                              options.dpi)
    # generate back of business card
    print("Generating back...")
    xml = gen.generate_back()
    if xml:
        if options.output == "svg":
            export.svg_to_file(xml, 'back.' + options.output)
        elif options.output == "cmyk_pdf":
            export.svg_to_cmyk_pdf(xml, 'back.pdf', options.height,
                                   options.width, options.bleed, options.unit,
                                   gen.rgb_to_cmyk)
        else:
            export.svg_to_pdf_png(xml, 'back.' + options.output,
                                  options.output, options.dpi)
    else:
        print("(no back generated)")
    print("Done.")
