###
# fedora-business-cards - for rendering Fedora contributor business cards
# Copyright (C) 2012  Red Hat, Inc. and others.
# Primary maintainer: Ian Weller <iweller@redhat.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
###

"""
Various business card generators can be placed here (i.e., a Fedora business
card layout, a Beefy Miracle business card layout).
"""

from fedora.client.fas2 import AccountSystem
from getpass import getpass

from fedora_business_cards import __version__
from fedora_business_cards import common


class BaseGenerator(object):
    fields = {}
    options = None
    height = None
    width = None
    bleed = None
    unit = None
    rgb_to_cmyk = None

    # These should be overridden by subclasses
    _gen_name = 'base'
    _gen_desc = 'Base generator'

    def __init__(self, options):
        self.options = options
        self.height = options.height
        self.width = options.width
        self.bleed = options.bleed
        if options.unit not in common.UNITS:
            raise KeyError(options.unit)
        self.unit = options.unit

    @classmethod
    def extra_options(cls, parser):
        option_group = parser.add_parser(cls._gen_name, help=cls._gen_desc)
        option_group.add_argument('-u', '--username', dest='username',
                                  default='', help='If set, use a different name'
                                  ' than the one logged in with to fill out'
                                  ' business card information')
        option_group.add_argument('-t', '--test', dest='test',
                                  action='store_true', help='If set, use test data'
                                  ' to fill out business card information')
        return option_group

    def collect_fas_information(self):
        if self.options.test:
            return {
                'human_name': 'Jane Doe',
                'gpg_keyid': '0xGPGKEYID',
                'ircnick': 'jane_doe',
                'username': self.options.username or 'jane_doe',
            }
        else:
            # ask for FAS login
            print("Login to FAS:")
            username = input("Username: ")
            password = getpass()

            # get information from FAS
            fas = AccountSystem(username=username, password=password,
                                useragent='fedora-business-cards/%s' % __version__)
            if self.options.username:
                username = self.options.username
            return fas.person_by_username(username)


    def collect_information(self):
        pass

    def generate_front(self):
        raise NotImplementedError()

    def generate_back(self):
        return None


__all__ = ('fedora','fedora-vertical','fedora-horizontal')
