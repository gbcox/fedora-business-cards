###
# fedora-business-cards - for rendering Fedora contributor business cards
# Copyright (C) 2012  Red Hat, Inc. and others.
# Primary maintainer: Brian Exelbierd <bexelbie@redhat.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
###

"""
Generator for the Fedora horizontal business card layout.

https://fedoraproject.org/wiki/Business_cards

https://pagure.io/design/issue/531
"""

import sys
import string
import importlib.resources as pkg_resources
import argparse
from decimal import Decimal
from xml.dom import minidom
from builtins import input

from fedora_business_cards import common
from fedora_business_cards.generators import BaseGenerator
from fedora_business_cards import templates  # relative-import the *package* containing the card templates

FEDORA_LOGO_VIEWBOX = '100 100 707.776 215.080'


class FedoraHorizontalGenerator(BaseGenerator):
    _gen_name = 'fedora-horizontal'
    _gen_desc = 'Fedora horizontal business cards'

    rgb_to_cmyk = {
            (60, 110, 180): (1, .46, 0, 0),
            (41, 65, 114): (1, .57, 0, .38),
            (0, 0, 0): (0, 0, 0, 1),
            (255, 255, 255): (0, 0, 0, 0),
    }

    def collect_information(self):
        userinfo = self.collect_fas_information()

        # set business card fields
        self.fields['name'] = userinfo["human_name"]
        self.fields['title'] = "Fedora Project Contributor"
        self.fields['lines'] = [''] * 6
        self.fields['lines'][0] = '%s@fedoraproject.org' % userinfo['username']
        self.fields['lines'][1] = 'fedoraproject.org'
        next_line = 2
        if userinfo['ircnick']:
            self.fields['lines'][next_line] = '%s on irc.freenode.net' % \
                    userinfo['ircnick']
            next_line += 1
        next_line += 1  # blank line

        if userinfo['gpg_keyid'] == None:
            gpg = ''
        else:
            gpg = "GPG: %s" % userinfo['gpg_keyid']
        self.fields['lines'][next_line] = gpg

        # ask user to edit information
        def cmdline_card_line(data):
            return "| %s%s |" % (data, ' ' * (59 - len(data)))

        # don't prompt user to edit in test mode
        done_editing = self.options.test
        while not done_editing:
            print("Current business card layout:")
            print("   +" + "-" * 61 + "+")
            print(" n " + cmdline_card_line(self.fields['name']))
            print(" t " + cmdline_card_line(self.fields['title']))
            print("   " + cmdline_card_line(''))
            for i in range(6):
                print((" %i " % i) + cmdline_card_line(self.fields['lines'][i]))
            print("   " + cmdline_card_line(''))
            print("   " + cmdline_card_line(''))
            print("   " + cmdline_card_line('fedora' + ' ' * 19 + \
                                            'FREEDOM. FRIENDS. '
                                            'FEATURES. FIRST.'))
            print("   +" + "-" * 61 + "+")
            lineno = input("Enter a line number to edit, or [y] to accept: ")
            if lineno == "" or lineno == "y":
                done_editing = True
            elif lineno in ['n', 't', '0', '1', '2', '3', '4', '5']:
                newdata = input("Enter new data for line %s: " % lineno)
                if lineno == 'n':
                    self.fields['name'] = newdata
                elif lineno == 't':
                    self.fields['title'] = newdata
                elif lineno in ['0', '1', '2', '3', '4', '5']:
                    self.fields['lines'][int(lineno)] = newdata

    def generate_front(self):
        card_template = string.Template(pkg_resources.read_text(templates, 'fedora-horizontal-front.svg-template'))
        biz_card = card_template.safe_substitute({'NAME': self.fields['name'],
                                                  'TITLE': self.fields['title'],
                                                  'Line0': self.fields['lines'][0],
                                                  'Line1': self.fields['lines'][1],
                                                  'Line2': self.fields['lines'][2],
                                                  'Line3': self.fields['lines'][3],
                                                  'Line4': self.fields['lines'][4],
                                                  'Line5': self.fields['lines'][5],
                                                  })
        return biz_card

    def generate_back(self):
        return pkg_resources.read_text(templates, 'fedora-back.svg-template')

generator = FedoraHorizontalGenerator
