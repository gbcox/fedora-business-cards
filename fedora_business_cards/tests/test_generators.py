#!/usr/bin/python3

import argparse
import decimal
import os
import tempfile
import unittest

from .. import common
from .. import export
from .. import generators

class TestGenerators(unittest.TestCase):

    def test_generate_output(self):
        options = argparse.Namespace(
            height=decimal.Decimal('2'),
            width=decimal.Decimal('3.5'),
            bleed=decimal.Decimal('0'),
            unit='in',
            dpi=300,
            username='',
            test=True,
        )
        # cmyk_pdf currently broken
        outputs = ['pdf', 'png', 'svg', 'eps']
        for genstr in generators.__all__:
            module = common.recursive_import('fedora_business_cards.generators.%s' % genstr)
            gen = module.generator(options)
            gen.collect_information()
            xml = gen.generate_front()
            with tempfile.TemporaryDirectory() as tmpdirname:
                for fmt in outputs:
                    filename = os.path.join(tmpdirname, 'front.' + fmt)
                    if fmt == "svg":
                        export.svg_to_file(xml, filename)
                    elif fmt == "cmyk_pdf":
                        export.svg_to_cmyk_pdf(xml, filename, options.height, options.width,
                                               options.bleed, options.unit, gen.rgb_to_cmyk)
                    else:
                        export.svg_to_pdf_png(xml, filename, fmt,
                                              options.dpi)
                    self.assertTrue(os.path.exists(filename), filename + " should be generated")
                    self.assertTrue(os.stat(filename).st_size > 0, filename + " should not be empty")

if __name__ == '__main__':
    unittest.main()
